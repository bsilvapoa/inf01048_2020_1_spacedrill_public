from controller_interface import ControllerInterface

class Controller(ControllerInterface):

    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Left
        2 - Right
        3 - Accelerate forward
        4 - Discharge 
        5 - Nothing
        """
        raise NotImplementedError

    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad 
             'drill_position': (x, y) 
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0  
             'enemy_1_drill_touching_asteroid': 0 or 1 
             'enemy_1_drill_touching_mothership': 0 or 1 
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """

        # print('len is', len(sensors))

        raise NotImplementedError

    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINTS: You can call self.run_episode (see controller_interface.py) to evaluate a given set of weights.
               The variable self.episode shows the number of times run_episode method has been called 

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        raise NotImplementedError
