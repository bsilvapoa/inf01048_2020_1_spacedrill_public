from config import EPISODE_LEN


class ControllerInterface:

    LEFT = 1
    RIGHT = 2
    ACCELERATE = 3
    DISCHARGE = 4
    NOTHING = 5

    def __init__(self, simulation, gs=None):
        self.gs = gs
        self.simulation = simulation
        self.episode = 0
        self.sensors = simulation.drill_1.sensors
        self.controller_2 = None
        self.controller_2_w = None

    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Left
        2 - Right
        3 - Accelerate forward
        4 - Discharge 
        5 - Nothing
        """
        raise NotImplementedError("This Method Must Be Implemented")

    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad 
             'drill_position': (x, y) 
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
                 'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0  
             'enemy_1_drill_touching_asteroid': 0 or 1 
             'enemy_1_drill_touching_mothership': 0 or 1 
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """
        raise NotImplementedError("This Method Must Be Implemented")

    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE
        
        HINTS: You can call self.run_episode (see controller_interface.py) to evaluate a given set of weights.
               The variable self.episode shows the number of times run_episode method has been called 
        
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        raise NotImplementedError("This function must be implemented")

    def run_episode(self, weights: tuple) -> int:

        self.episode += 1
        self.simulation.reset()
        self.sensors = self.simulation.frame_step(0)
        frame_current = 0
        episode_length = EPISODE_LEN

        while frame_current <= episode_length:
            self.sensors = self.simulation.frame_step(self.take_action(weights),
                                                      self.controller_2.take_action(self.controller_2_w))
            self.controller_2.sensors = self.simulation.drill_2.sensors
            frame_current += 1
            if self.gs is not None:
                self.gs.draw()

        score = self.simulation.drill_1.resources

        return score

    def set_controller_2_for_learn_mode(self, controller_2, w2):
        self.controller_2 = controller_2
        self.controller_2_w = w2

    def set_gs(self, gs):
        self.gs = gs

